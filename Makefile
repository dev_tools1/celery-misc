rabbitmq:
	docker run -d -p 5672:5672 rabbitmq
celery-worker-server:
	celery -A proj worker --loglevel=INFO
redis:
	docker run -d -p 6379:6379 redis
