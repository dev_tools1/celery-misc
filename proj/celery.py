from celery import Celery

app = Celery("proj",
        broker="amqp://guest@localhost:5672//",
        backend="rpc://guest@localhost:5672//",
              include=["proj.tasks"])

app.conf.update(
    result_expires=3600,
        )

if __name__ == 'main':
    app.start()

